//Validate the Email 
//You are given a string, a user input for their email, make sure it is in correct email format: foo@bar.baz
//Hints: Use rubular to check a few, use regexp text method 
var email = 'foo@bar.baz'; 
 
var format = /^\w+@\w+\.\w{2,4}$/; 

const valEmail = format.test(email);

console.log(valEmail); 


// Create attack function below.  This will take the following parameters:
// attackingPlayer, defendingPlayer, baseDamage, variableDamage
function attack (attackingPlayer, defendingPlayer, baseDamage, variableDamage){
    totalDamage = baseDamage + Math.ceil(Math.random(), variableDamage);
    defendingPlayer.health -= totalDamage; 
    return `${attackingPlayer.name} hits ${defendingPlayer.name} for ${totalDamage} damage`; 
}


// Create player1 and player2 objects below
// Each should have a name property of your choosing, and health property equal to 10
let player1 = {
    name: "Roxie", 
    health: 10
}

let player2 = {
    name: "Marcus", 
    health:  10
}

// DO NOT MODIFY THE CODE BELOW THIS LINE
// Set attacker and defender.  Reverse roles each iteration
let attackOrder = [player1, player2];

// Everything related to preventInfiniteLoop would not generally be necessary, just adding to
// safeguard students from accidentally creating an infinite loop & crashing browser
let preventInfiniteLoop = 100;
while (player1.health >= 1 && player2.health >= 1 && preventInfiniteLoop > 0) {
  const [attackingPlayer, defendingPlayer] = attackOrder;
  console.log(attack(attackingPlayer, defendingPlayer, 1, 2));
  attackOrder = attackOrder.reverse();

  preventInfiniteLoop--;
}
const eliminatedPlayer = player1.health <= 0 ? player1 : player2;
console.log(`${eliminatedPlayer.name} has been eliminated!`);

// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price
function logReceipt(){
  let sum = 0; 
  const reciptItems = Array.from(arguments); 
  reciptItems.forEach(function(item){
      sum += item.price; 
      console.log (`${item.descr}-${item.price}`); 
  });

  console.log(`Total Price is ${sum}`);
};

// Check
logReceipt(
  {descr: 'Burrito', price: 5.99},
  {descr: 'Chips & Salsa', price: 2.99},
  {descr: 'Sprite', price: 1.99}
  );
  // should log something like:
  // Burrito - $5.99
  // Chips & Salsa - $2.99
  // Sprite - $1.99
  // Total - $10.97

//1. Create a constructor function SpaceShip. It should set  two properties: name and topSpeed. 
//It should also have a method accelerate that logs to the console`${name} moving to ${topSpeed}`.
//2. We don't want anyone to access name and topSpeed, make these private!
//3. Well, maybe we should let people change the topSpeed – but not the ship name! Keep both of
//these private, but add a method that changes the topSpeed.
//4. Call the constructor with a couple ships, change  the topSpeed, and call the accelerate method. Woohoo!

const SpaceShip =  function SpaceShip(name, topSpeed){
  const shipName = name; 
  let shipTopSpeed = topSpeed; 
  this.accelerate = function (){
    console.log(`${shipName} moving to ${shipTopSpeed}`); 
  }; 
  this.ChangeSpeed = function(newSpeed) {shipTopSpeed = newSpeed;} 
}; 

const RoxieShip = new SpaceShip('Roxie Destroyer', '100mph'); 
const MarcusShip = new SpaceShip('Marcus Destroyer', '150mph'); 
RoxieShip.accelerate(); 
MarcusShip.accelerate(); 
MarcusShip.ChangeSpeed("200mph"); 
MarcusShip.accelerate(); 

