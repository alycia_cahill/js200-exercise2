// You are given a phone number as a string, with one of the 
// following formats (but you don't know which one!)
// '(206) 333-4444'
// '206-333-4444'
// '206 333 4444'


// Create a function testPhoneNumber that takes in a phoneNumber string 
// in one of the above formats.  This should use a regular expression
// and run the test method to determine if the number is valid
// Returns the result of the test method call (either true or false)
function testPhoneNumber (phoneNumber) {
    var format = /(?:\d{3}|\(\d{3}\))[\s-]\d{3}[\s-]\d{4}/; 
    let valEmail = format.test(phoneNumber); 
    return valEmail;
}; 

// Check testPhoneNumber
testPhoneNumber('206-333-4444');  // returns true
testPhoneNumber('206-12-3456');  // returns false

testPhoneNumber('(206) 333-4444');  // returns true
testPhoneNumber('206 333 4444');  // returns true
testPhoneNumber('206 333-4444');  // returns true

testPhoneNumber('(206 333-4444');  // returns false


// Create a function parsePhoneNumber that takes in a phoneNumber string 
// in one of the above formats.  For this, you can assume the phone number
// passed in is correct.  This should use a regular expression
// and run the exec method to capture the area code and remaining part of
// the phone number.
// Returns an object in the format {areaCode, phoneNumber}
function parsePhoneNumber(phoneNumber){
    var format = /(\d{3}|\(\d{3}\))[\s-](\d{3}[\s-]\d{4})/; 
    var toRemove = /\D/g; 
    const phoneArray = format.exec(phoneNumber); 
    return {
       areaCode : phoneArray[1].replace(toRemove, ''),
       phoneNumber : phoneArray[2].replace(toRemove, '')
    }
}


// Check parsePhoneNumber
parsePhoneNumber('206-333-4444');  
// returns {areaCode: '206', phoneNumber: '3334444'}

parsePhoneNumber('(222) 422-5353');
// returns {areaCode: '222', phoneNumber: '4225353'}