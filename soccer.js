(function(){
  const RESULT_VALUES = {
    w: 3,
    d: 1,
    l: 0
  }

  // This function accepts one argument, the result, which should be a string
  // Acceptable values are 'w', 'l', or 'd'
  function getPointsFromResult(result) {
    return RESULT_VALUES[result];
  }

  // Create getTotalPoints function which accepts a string of results
  // including wins, draws, and losses i.e. 'wwdlw'
  // Returns total number of points won
  function getTotalPoints (results){
      const resultsArray = results.split(''); 
      let sum = 0; 
      resultsArray.forEach(function(result){
        let points = getPointsFromResult(result);
        sum += points; 
      }); 
      return sum; 

  }


  // Check getTotalPoints
  console.log(getTotalPoints('wwdl')); // should equal 7

  // create orderTeams function that accepts as many team objects as desired, 
  // each argument is a team object in the format { name, results }
  // i.e. {name: 'Sounders', results: 'wwlwdd'}
  // Logs each entry to the console as "Team name: points"
  function orderTeams(){ 
    const team = Array.from(arguments); 
    team.forEach(function(theTeam){
      const thePoints = getTotalPoints(theTeam.results); 
      console.log(`${theTeam.name}: ${thePoints}`)
    }); 
  }




  // Check orderTeams
  orderTeams(
    {name: 'Sounders', results: 'wwdl'},
    {name: 'Galaxy', results: 'wlld'}
  ); 
  // should log the following to the console:
  // Sounders: 7
  // Galaxy: 4
})(); 