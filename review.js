//1. In class assignments 
// Week 2 In Class Excersises
// Objects 
//1. Create an Object Prepresentaion of yourself.  Should include:
// firstName 
// lastName 
// 'favorite food'
// mom & dad (objects with the same 3 properties as above) 


const me = {
	firstName: 'Alycia', 
	lastName: 'Cahill', 
	'favorite food': 'tacos',
	mom: {
		firstName: 'Sue', 
		lastName: 'Mott', 
		'favorite food': 'pasta' }, 
	dad: {
		firstName: 'James', 
		lastName: 'Cahill', 
		'favorite food': 'burgers'}
}; 

//2. console.log dad's firstName, mom's favorite food 
console.log(me.dad.firstName); 
console.log(me.mom['favorite food']); 

//Create an array to represent this Tic-Tac-Toe Board

var board = [
	'-', 
	'O', 
	'-', 
	'-', 
	'X', 
	'O', 
	'X', 
	'-', 
	'X'
]; 
//After the array is created, O claims the top right square.  Update that value.
board[0] = 'O'; 


//Log the grid to the console.Hint: log each row separately.
console.log(board[0], board[1], board[2]);
console.log(board[3], board[4], board[5]); 
console.log(board[6], board[7], board[8]); 

//2. Assignment Date
const assignmentDate = '1/21/2019'; 

// Convert to a Date instance
let dateArray = assignmentDate.split('/'); 
let asMonth = Number(dateArray[0]) - 1;
let asDay = Number(dateArray[1]);
let asYear = Number(dateArray[2]);
let updtedAssignemtnDate = new Date(asYear, asMonth, asDay); 
let asDateMilliSeconds = updtedAssignemtnDate.getTime(); 

// Create dueDate which is 7 days after assignmentDate. Create the due date as a Date instance (new Date).
let addDays =  86400000 * 7; 
let dueDateMilliseconds = asDateMilliSeconds + addDays; 
let dueDate = new Date(dueDateMilliseconds); 


// Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// log this value using console.log
let isoDate = dueDate.toISOString(); 
let isoFormatted = isoDate.slice(0, 10); // returns first format 

//From WC3 Schoools 
var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";

//Back to my code
let dueDateMonth = month[dueDate.getMonth()]; 
let dueDateDate = dueDate.getDate(); 
let dueDateYear = dueDate.getFullYear(); 
let dateString = '<time datetime="' + isoFormatted + '">' + dueDateMonth + ' ' + dueDateDate + ', ' + dueDateYear + '</time>'; 
console.log(dateString); 
